#!/bin/bash

react-deploy-s3 deploy \
--region ap-southeast-2 \
--bucket weblogapi.web.static \
--access-key-id $1 \
--secret-access-key $2