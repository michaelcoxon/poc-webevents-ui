import React, { useState } from "react";

import axios from "axios";

interface StepThreeProps {
  onNext: () => void;
}

const constructBody = (email: string) => {
  if (!email) {
    return undefined;
  }

  if (!email.includes("@")) {
    return {
      clid: "1000000000000001",
      status: "COMPLETED",
      badData: "a problem",
    };
  }

  return {
    clid: "1000000000000001",
    status: "COMPLETED",
    goodData: "not a problem",
  };
};

const StepThree = (props: StepThreeProps): React.ReactElement => {
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");

  const submit = async (event: React.FormEvent<HTMLButtonElement>) => {
    event.preventDefault();

    const body = constructBody(email);

    try {
      const res = await axios.post(
        `${process.env.REACT_APP_TEST_API_BASE_URL}/form`,
        body
      );

      if (res.status >= 200 && res.status < 300) {
        props.onNext();
      } else {
        setError("Unable to save form.");
      }
    } catch (error) {
      setError("Unable to save form.");
    }
  };

  return (
    <>
      <form>
        <div>
          <label htmlFor="email">Email: </label>
          <input
            type="text"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>

        <button type="submit" onClick={submit}>
          Next
        </button>
      </form>
      <div>{error}</div>
    </>
  );
};

export default StepThree;
