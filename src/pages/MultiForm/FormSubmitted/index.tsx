import React from "react";

const FormSubmitted = (): React.ReactElement => {
  return <div>The form was successfully submitted</div>;
};

export default FormSubmitted;
