import React, { useState } from "react";

import axios from "axios";

interface StepOneProps {
  onNext: () => void;
}

const StepOne = (props: StepOneProps): React.ReactElement => {
  const [address, setAddress] = useState("");
  const [error, setError] = useState("");

  const submit = async (event: React.FormEvent<HTMLButtonElement>) => {
    event.preventDefault();

    const body = address
      ? {
          clid: "1000000000000001",
          status: "INPROGRESS",
          goodData: "not a problem",
        }
      : {};

    try {
      const res = await axios.post(
        `${process.env.REACT_APP_TEST_API_BASE_URL}/form`,
        body
      );

      if (res.status >= 200 && res.status < 300) {
        props.onNext();
      } else {
        setError("Unable to save form.");
      }
    } catch (error) {
      setError("Unable to save form.");
    }
  };

  return (
    <>
      <form>
        <div>
          <label htmlFor="address">Address: </label>
          <input
            type="text"
            id="address"
            value={address}
            onChange={(e) => setAddress(e.target.value)}
          />
        </div>

        <button type="submit" onClick={submit}>
          Next
        </button>
      </form>
      <div>{error}</div>
    </>
  );
};

export default StepOne;
