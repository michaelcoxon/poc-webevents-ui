import React from "react";

import { Route, Switch, useHistory, useRouteMatch } from "react-router-dom";

import { LazyLoad } from "web-logger-lib";

import FormSubmitted from "./FormSubmitted";

const StepThree = LazyLoad(() => import("./StepThree"), "StepThree");
const StepTwo = LazyLoad(() => import("./StepTwo"), "StepTwo");
const StepOne = LazyLoad(() => import("./StepOne"), "StepOne");

const MultiForm = (): React.ReactElement => {
  const match = useRouteMatch();
  const history = useHistory();

  return (
    <Switch>
      <Route path={`${match.path}/step-two`}>
        <StepTwo onNext={() => history.push(`${match.path}/step-three`)} />
      </Route>

      <Route path={`${match.path}/step-three`}>
        <StepThree
          onNext={() => history.push(`${match.path}/form-submitted`)}
        />
      </Route>

      <Route path={`${match.path}/form-submitted`}>
        <FormSubmitted />
      </Route>

      <Route path={`${match.path}`}>
        <StepOne onNext={() => history.push(`${match.path}/step-two`)} />
      </Route>
    </Switch>
  );
};

export default MultiForm;
