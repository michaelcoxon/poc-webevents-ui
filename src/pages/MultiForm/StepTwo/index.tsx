import React, { useState } from "react";

import axios from "axios";

interface StepTwoProps {
  onNext: () => void;
}

const StepTwo = (props: StepTwoProps): React.ReactElement => {
  const [mobile, setMobile] = useState("");
  const [error, setError] = useState("");

  const submit = async (event: React.FormEvent<HTMLButtonElement>) => {
    event.preventDefault();

    const body = mobile
      ? {
          clid: "1000000000000001",
          status: "INPROGRESS",
          goodData: "not a problem",
        }
      : {};

    try {
      const res = await axios.post(
        `${process.env.REACT_APP_TEST_API_BASE_URL}/form`,
        body
      );

      if (res.status >= 200 && res.status < 300) {
        props.onNext();
      } else {
        setError("Unable to save form.");
      }
    } catch (error) {
      setError("Unable to save form.");
    }

    props.onNext(); //*** */
  };

  return (
    <>
      <form>
        <div>
          <label htmlFor="mobile">Mobile: </label>
          <input
            type="text"
            id="mobile"
            value={mobile}
            onChange={(e) => setMobile(e.target.value)}
          />
        </div>

        <button type="submit" onClick={submit}>
          Next
        </button>
      </form>
      <div>{error}</div>
    </>
  );
};

export default StepTwo;
