import React, { useState } from "react";

import axios from "axios";

const Login = (): React.ReactElement => {
  const [loginMessage, setLoginMessage] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const submit = async (event: React.FormEvent<HTMLButtonElement>) => {
    event.preventDefault();

    setLoading(true);

    try {
      const res = await axios.post(
        `${process.env.REACT_APP_TEST_API_BASE_URL}/login`,
        {
          clid: username,
          password,
        }
      );

      if (res.status === 200) {
        setLoginMessage("Login successful!");
      } else {
        setLoginMessage(`Login failure: ${res.status}`);
      }
    } catch (error) {
      setLoginMessage("Login failure");
    }

    setLoading(false);
  };

  if (loading) {
    return <>Logging in...</>;
  }

  return (
    <>
      <form>
        <div>
          <label htmlFor="username">CLID: </label>
          <input
            type="text"
            id="username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>

        <div>
          <label htmlFor="password">Password: </label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>

        <button type="submit" onClick={submit}>
          Login
        </button>
      </form>
      <div>{loginMessage}</div>
    </>
  );
};

export default Login;
