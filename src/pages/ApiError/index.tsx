import React from "react";

import axios from "axios";

interface ApiErrorProps {}

interface ApiErrorState {
  hasErrors: boolean;
  isLoading: boolean;
}

class ApiError extends React.Component<ApiErrorProps, ApiErrorState> {
  readonly state = { hasErrors: false, isLoading: true };

  componentDidMount() {
    axios
      .post(`${process.env.REACT_APP_TEST_API_BASE_URL}/form`, {
        clid: "10000000000001",
        doError: true,
        status: "INPROGRESS",
      }) // should return an API error.
      .then(() => {
        this.setState({ hasErrors: true, isLoading: false });
      })
      .catch(() => {
        this.setState({ hasErrors: true, isLoading: false });
      });
  }

  render() {
    let message = "";

    if (this.state.isLoading) {
      message = "Fetching data from server...";
    } else if (this.state.hasErrors) {
      message = "Unable to fetch data from the server due API error.";
    } else {
      message = "Hmm, an api error should have been thrown by now...";
    }

    return <>{message}</>;
  }
}

export default ApiError;
