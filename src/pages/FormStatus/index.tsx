import React, { useState } from "react";

import axios from "axios";

const FormStatus = (): React.ReactElement => {
  const [clid, setClid] = useState("");
  const [error, setError] = useState("");
  const [clidStatus, setClidStatus] = useState("");

  const submit = async (event: React.FormEvent<HTMLButtonElement>) => {
    event.preventDefault();

    try {
      const res = await axios.get(
        `${process.env.REACT_APP_TEST_API_BASE_URL}/form/${clid}`
      );

      if (res.status === 400) {
        setError("Invalid clid :(");
      } else {
        setClidStatus(res.data.body["status"]);
      }
    } catch (error) {
      if (
        error &&
        error.response &&
        error.response.status &&
        error.response.status === 400
      ) {
        setError("Invalid clid :(");
      } else {
        setError("Something went wrong.");
      }
    }
  };

  return (
    <>
      <form>
        <div>
          <label htmlFor="clid">CLID: </label>
          <input
            type="text"
            id="clid"
            value={clid}
            onChange={(e) => setClid(e.target.value)}
          />
        </div>

        <button type="submit" onClick={submit}>
          Check status
        </button>
      </form>
      <div>{error ? error : `Form status: ${clidStatus}`}</div>
    </>
  );
};

export default FormStatus;
