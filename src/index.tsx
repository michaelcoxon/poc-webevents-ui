import ReactDOM from "react-dom";
import axios from "axios";

import * as Logger from "web-logger-lib";

import "./index.css";
import App from "./App";

axios.defaults.headers.post["Content-Type"] = "application/json";

Logger.init({
  endpoint: process.env.REACT_APP_LOGGER_API_URL || "",
  trackUncaughtErrors: true,
  trackClickEvents: true,
  flushSize: 15,
  flushTimer: 60000,
  maxBufferSize: 30,
});

Logger.trackAllAxiosRequests(axios);

// Capture CLID and stamp it on all subsequent events.
setTimeout(() => {
  Logger.updateContextForEvent({
    CLID: "56789",
  });
}, 2000); // Simulate long running request so that initial events do not have the CLID stamp.

const Root = Logger.withNonLazyComponentProfiler(App, "Application"); // Profile the root component.

ReactDOM.render(<Root />, document.getElementById("root"));
