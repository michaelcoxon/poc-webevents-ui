import styled from "styled-components";

import { Link } from "react-router-dom";

const StyledLink = styled(Link)`
  display: block;
`;

const Nav = styled("nav")`
  margin-bottom: 30px;
`;

export { StyledLink as Link, Nav };
