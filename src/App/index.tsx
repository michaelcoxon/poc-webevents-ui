import React, { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { UrlListener, LazyLoad } from "web-logger-lib";

import { Link, Nav } from "./styles";

const Login = LazyLoad(() => import("../pages/Login"), "Login");
const ApiError = LazyLoad(() => import("../pages/ApiError"), "ApiError");
const Error = LazyLoad(() => import("../pages/Error"), "Error");
const MultiForm = LazyLoad(() => import("../pages/MultiForm"), "MultiForm");
const FormStatus = LazyLoad(() => import("../pages/FormStatus"), "FormStatus");

const App = () => {
  return (
    <Router basename="/">
      <UrlListener />
      <Suspense fallback={<div>Loading page...</div>}>
        <Nav>
          <Link to="/">Home</Link>
          <Link to="/login">Login</Link>
          <Link to="/error">Error</Link>
          <Link to="/api-error">API Error</Link>
          <Link to="/multi-form">Form with multiple steps</Link>
          <Link to="/form-status">Form Status</Link>
        </Nav>
        <Switch>
          <Route path="/api-error" component={ApiError} />
          <Route path="/error" component={Error} />
          <Route path="/login" component={Login} />
          <Route path="/multi-form" component={MultiForm} />
          <Route path="/form-status" component={FormStatus} />
          <Route path="/">
            <div>Click on one of the routes above to test various cases.</div>
          </Route>
        </Switch>
      </Suspense>
    </Router>
  );
};

export default App;
